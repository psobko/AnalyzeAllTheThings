//
//  ViewController.swift
//  AnalyzeAllTheThings
//
//  Created by Peter Sobkowski on 2018-05-24.
//  Copyright © 2018 psobko. All rights reserved.
//

import UIKit
import Foundation; import CoreImage

struct SomeException:LocalizedError{
    public let errorDescription: String?
}

let goodConstant = 42
let bad_constant = 42

enum CompassPoint {
    case north
    case south
    case east
    case west
}

enum BadCompassPoint {
    case N0rth
    case Sou_th
    case East
    case WEST
}

class ViewController: UIViewController {

    @IBOutlet var passwordField: UITextField!
    @IBOutlet var userField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let someVariable = 1
        let Some_Var1able = 1
        print("\(someVariable) \(Some_Var1able)")
        // Do any additional setup after loading the view, typically from a nib.
    }

    class Movie{
        var name:String = "Movie Title"
        var director:String = "Famous Director"
    }
    
    func forcedTypeCast(item:Any?) {
    
        if let movie = item as? Movie {
            print("Movie: '\(movie.name)', dir. \(movie.director)")
        }
        
        let movie = item as! Movie
        print("Movie: '\(movie.name)', dir. \(movie.director)")

    }
    
    func variableDeclerations() {
        let x: Int = 2
        let z : Int = 3
        let y:   String = "abc"
        print("\(x,y,z)")
    }

    
}

// TODOs

// TODO: <insert mandatory todo comment>

/// TODO: Documentation comments should not have TODOs

//// TODO: Nested comments should not have TODOs

// //TODO: Nested comments should not have TODOs

// TODO: Nested comments should not have TODOs // some comment

//// TODO: Nested comments should not have TODOs


//Semicolon

class SomeBadClass {
    func semiColon() {
       
        
        // while loop
        while true {
            break
        };
        
        // repeat while
        repeat {
            break
        } while true;
    }
};

class SomeClass {
    func semiColon() {
        // while loop
        while true {
            break
        }
        
        // repeat while
        repeat {
            break
        } while true
       
    }
}

protocol SomeProtocol {
    var SomeMethod: String { get }
    func SomeMethod(f: Int)
    func SomeMethod(bar: String, baz: Double)
}

protocol SomeBadrotocol {
    var SomeMethod: String { get };
    func SomeMethod(f: Int);
    func SomeMethod(bar: String, baz: Double);
};

// Brace Style

enum SomeEnum {
    case A, B, C, D
}

enum SomeEnum1 {
    case A
    case B
    case C
    case D
}

enum SomeEnum2: Int {
    case A, B, C = 5, D
}

enum SomeBadEnum
{
    case A, B, C, D
}

enum SomeBadEnum1
{
    case A
    case B
    case C
    case D
}

enum SomeBadEnum2: Int
{
    case A, B, C = 5, D
}

// Whitespace
func function1() {
    var text = 1
        text = 2
}


// a comment
func function2() {
    // something goes here
}

struct SomeStruct {
    
    func function3() {
        // something goes here
    }
    
    func function4() {
        // something else goes here
    };
    
}

func function5() {
    // something goes here
}

func badFunction1() {
    var text = 1
        text = 2
}
// a comment
func badFunction2() {
    // something goes here
}

struct SomeBadStruct {
    func badFunction3() {
        // something goes here
    }
    
    func badFunction4() {
        // something else goes here
    };
}
func badFunction5() {
    // something goes here
}
